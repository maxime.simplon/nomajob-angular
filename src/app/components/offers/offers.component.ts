import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-offers',
  templateUrl: 'offers.component.html',
  styleUrls: ['offers.component.css']
})
export class OffersComponent {
  offers: Observable<any[]>;
  constructor(db: AngularFirestore) {
    this.offers = db.collection('offers').valueChanges();
  }
 
}